
<div style="border-bottom: 0.009em solid #949494; width: 100%;">
    <div class="container letraEuropaLight d-none d-sm-block" style="margin-top: 5px; margin: right 15px;">
        <div class="col-lg-12 col-md-12 col-sm-12 ">
            <span class="float-sm-start">
                <span class="header-st"> <a href="https://www.instagram.com/reymalfoto/" target="_blank" style="text-decoration: none; color: black;">Instagram</a> </span>
                <span>&nbsp;&nbsp;</span>
                <span class="header-st"> <a href="https://web.facebook.com/ReyMal097" target="_blank" style="text-decoration: none; color: black;">Facebook</a> </span>
            </span>
            <span class="float-sm-end">
                <span class="header-st"> <a href="agenda.php" style="text-decoration: none; color: black;">Agenda</a> </span>
                <span>&nbsp;&nbsp;</span>
                <span class="header-st"> <a href="paquetes.php" style="text-decoration: none; color: black;">Paquetes</a> </span>
            </span>
        </div>
    </div>
    <div class="container d-none d-sm-block" style="margin-top: 5px; margin-bottom: 5px;">
        <!-- <img src="/img/rm-black.png" style="height: 150px;" class="d-flex justify-content-center text-center mx-auto Home_Button" alt=""> -->
        <a href="index.php"><img src="img/rm-black-2.png" alt="" class="d-flex justify-content-center text-center mx-auto Home_Button logo-st"></a>

    </div>
    <nav class="navbar  navbar-expand-lg navbar-light bg-whith ">
        <div class="container-fluid d-flex">
            <!-- <a class="navbar-brand " href="#">Navbar</a> -->
            <nav class="navbar navbar-light bg-whith movilLogo">
                <div class="container movilLogo">
                    <a class="navbar-brand" href="index.php">
                        <img id="test" src="img/rm-black-2.png" alt="" width="150px" height="auto">
                    </a>
                </div>
            </nav>
            <nav class="navbar navbar-light bg-whith movilLogo ms-auto">
                <div class="container movilLogo ">
                    <a class="navbar-brand" href="index.php">
                        <a href="https://www.instagram.com/reymalfoto/" target="_blank"><img id="test" src="img/instagram.png" alt="" width="40px" height="auto"></a>
                        <a href="https://web.facebook.com/ReyMal097" target="_blank"><img id="test" src="img/facebook.png" alt="" width="40px" height="auto"></a>
                    </a>
                </div>
            </nav>

            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <!-- <a href="../ReyMal/index.php"><img src="img/rm-black-2.png" alt="" id="buttonLogo" class="d-flex justify-content-center text-center mx-auto Home_Button logo-st"></a> -->
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav  mx-auto mb-2 mb-lg-0">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle header-st active" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            PORTAFOLIO
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li class=" header-st-dw"><a class="dropdown-item" href="bautizo.php">Bautizo</a></li>
                            <li class=" header-st-dw"><a class="dropdown-item" href="boda.php">Bodas</a></li>
                            <!-- <li></li>
                                <hr class="dropdown-divider">
                            </li> -->
                            <li class=" header-st-dw"><a class="dropdown-item" href="casual.php">Casual</a></li>
                            <li class=" header-st-dw"><a class="dropdown-item" href="editorial.php">Editorial</a></li>
                            <li class=" header-st-dw"><a class="dropdown-item" href="xv.php">XV Años</a></li>
                            <li class=" header-st-dw"><a class="dropdown-item" href="productos.php">Producto</a></li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link  header-st" aria-current="page" href="testimonios.php">TESTIMONIOS</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link header-st" aria-current="page" href="entregas.php">ENTREGAS</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link header-st" aria-current="page" href="sobrenosotros.php">SOBRE NOSOTROS</a>

                    </li>
                    <li class="acerca nav-item">
                        <a class="nav-link header-st" aria-current="page" href="paquetes.php">PAQUETES</a>
                    </li>
                    <li class="acerca nav-item">
                        <a class="nav-link header-st" aria-current="page" href="agenda.php">AGENDA</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</div>