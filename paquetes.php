<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <title>Paquetes</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Bootstrap CSS -->
    <link rel="shortcut icon" href="img/icon_camara.png" type="image/x-icon">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/estilos.css">
    <link rel="stylesheet" type="text/css" href="css/header.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Cinzel&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="css/paquetes.css">
</head>

<body>
    <header>
        <?php
        include 'header.php'
        ?>
    </header>

    <div id="mainboxs">
        <h2 class="header-st" style="font-size: 30px;">Bautizo</h2>
        <div id="foto1">
            <img src="img/paquetes/Bautizo/1.png" id="img">
        </div>
        <div id="foto2">
            <img src="img/paquetes/Bautizo/2.png" id="img">
        </div>
        <div id="foto3">
            <img src="img/paquetes/Bautizo/3.png" id="img">
        </div>
        <div id="foto4">
            <img src="img/paquetes/Bautizo/4.png" id="img">
        </div>
    </div>

    <div id="mainboxs">
        <h2 class="header-st" style="font-size: 30px;">Boda</h2>
        <div id="foto1">
            <img src="img/paquetes/Boda/1.png" id="img">
        </div>
        <div id="foto2">
            <img src="img/paquetes/Boda/2.png" id="img">
        </div>
        <div id="foto3">
            <img src="img/paquetes/Boda/3.png" id="img">
        </div>
        <div id="foto4">
            <img src="img/paquetes/Boda/4.png" id="img">
        </div>
    </div>

    <div id="mainboxs">
        <h2 class="header-st" style="font-size: 30px;">Casual</h2>
        <div id="foto1">
            <img src="img/paquetes/Casual/1.png" id="img">
        </div>
        <div id="foto2">
            <img src="img/paquetes/Casual/2.png" id="img">
        </div>
        <div id="foto3">
            <img src="img/paquetes/Casual/3.png" id="img">
        </div>
    </div>

    <div id="mainboxs">
        <h2 class="header-st" style="font-size: 30px;">Embarazo</h2>
        <div id="foto1">
            <img src="img/paquetes/Embarazo/1.png" id="img">
        </div>
        <div id="foto2">
            <img src="img/paquetes/Embarazo/2.png" id="img">
        </div>
        <div id="foto3">
            <img src="img/paquetes/Embarazo/3.png" id="img">
        </div>
    </div>

    <div id="mainboxs">
        <h2 class="header-st" style="font-size: 30px;">Infantil</h2>
        <div id="foto1">
            <img src="img/paquetes/Infantil/1.png" id="img">
        </div>
        <div id="foto2">
            <img src="img/paquetes/Infantil/2.png" id="img">
        </div>

        <div id="foto3">
            <img src="img/paquetes/Infantil/3.png" id="img">
        </div>
        <div id="foto4">
            <img src="img/paquetes/Infantil/4.png" id="img">
        </div>
    </div>

    <div id="mainboxs">
        <h2 class="header-st" style="font-size: 30px;">Evento Infantil</h2>
        <div id="foto2">
            <img src="img/paquetes/Infantil/5.png" id="img">
        </div>
        <div id="foto3">
            <img src="img/paquetes/Infantil/6.png" id="img">
        </div>
    </div>

    <div id="mainboxs">
        <h2 class="header-st" style="font-size: 30px;">Pareja</h2>
        <div id="foto2">
            <img src="img/paquetes/Pareja/1.png" id="img">
        </div>
        <div id="foto3">
            <img src="img/paquetes/Pareja/2.png" id="img">
        </div>
    </div>

    <div id="mainboxs">
        <h2 class="header-st" style="font-size: 30px;">Productos</h2>
        <div id="foto1">
            <img src="img/paquetes/Productos/1.png" id="img">
        </div>
        <div id="foto2">
            <img src="img/paquetes/Productos/2.png" id="img">
        </div>
        <div id="foto3">
            <img src="img/paquetes/Productos/3.png" id="img">
        </div>
    </div>

    <div id="mainboxs">
        <h2 class="header-st" style="font-size: 30px;">XV Años</h2>
        <div id="foto2">
            <img src="img/paquetes/XV/1.png" id="img">
        </div>
        <div id="foto3">
            <img src="img/paquetes/XV/2.png" id="img">
        </div>
    </div>

    <div id="mainboxs">
        <h2 class="header-st" style="font-size: 30px;">XV Años by Succes (SOMBRERETE)</h2>
        <div id="foto2">
            <img src="img/paquetes/XV/3.png" id="img">
        </div>
        <div id="foto3">
            <img src="img/paquetes/XV/4.png" id="img">
        </div>
        <div id="foto4">
            <img src="img/paquetes/XV/5.png" id="img">
        </div>
    </div>

    <footer>
      <?php
      include 'footer.php'
      ?>
    </footer>
    
    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    
</body>


</html>