<html>
<head>
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Cinzel&display=swap" rel="stylesheet">
<!-- ----------------------componentes ecenciales------------------------------------------- -->
<link rel="shortcut icon" href="img/icon_camara.png" type="image/x-icon">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Bootstrap CSS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="css/estilos.css">
  <link rel="stylesheet" type="text/css" href="css/header.css">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Cinzel&display=swap" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <!-- --------------------------------------- -->
<style>

div.gallery {
  margin: 10px;
  border: 5px solid #ccc;
  float: left;
  width: 300px;
}
div.gallery:hover {
  border: 1px solid #777;
}

div.gallery img {
  width: 100%;
  height: auto;
}

div.desc {
  padding: 15px;
  text-align: center;
  font-family: 'Cinzel', serif;
}
div.a { 
    text-align: center;
    font-family: 'Cinzel', serif;
}

</style>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
     integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" 
     crossorigin="anonymous">
    
    <link rel="stylesheet" type="text/css" href="css/estilos.css">
</head>

<body>
<?php
        include 'header.php'
?>

<div class="a">
<h2 > Editorial </h2>
</div>

<div class="gallery" >
  <a target="_blank" href="img\editorial\1.jpg">
    <img src="img\editorial\1.jpg" alt="1" width="600" height="400">
  </a>
  <div class="desc"> Galilea & Ashley <br> Vicente Guerrero Dgo.<br></div>
</div>

<div class="gallery" >
  <a target="_blank" href="img\editorial\1a.jpg">
    <img src="img\editorial\1a.jpg" alt="1" width="600" height="400">
  </a>
  <div class="desc"> Flor <br> Durango Dgo.<br></div>
</div>

<div class="gallery">
  <a target="_blank" href="img\editorial\2.jpg">
    <img src="img\editorial\2.jpg" alt="2" width="600" height="400">
  </a>
  <div class="desc"> Paola <br> Hacienda la Galera Durango Dgo.<br></div>
</div>

<div class="gallery">
  <a target="_blank" href="img\editorial\3.jpg">
    <img src="img\editorial\3.jpg" alt="3" width="600" height="400">
  </a>
  <div class="desc"> Paola, Regina & Andira <br> Hacienda la Galera Durango Dgo.<br></div>
</div>

<div class="gallery">
  <a target="_blank" href="img\editorial\4.jpg">
    <img src="img\editorial\4.jpg" alt="Mountains" width="600" height="400">
  </a>
  <div class="desc"> Ariana <br> Sombrerete Zac.<br></div>
</div>
<div class="gallery">
  <a target="_blank" href="img\editorial\5.jpg">
    <img src="img\editorial\5.jpg" alt="Mountains" width="600" height="400">
  </a>
  <div class="desc"> Avril <br> Sombrerete Zac.<br></div>
</div>
<div class="gallery">
  <a target="_blank" href="img\editorial\6.jpg">
    <img src="img\editorial\6.jpg" alt="Mountains" width="600" height="400">
  </a>
  <div class="desc"> Susana <br>Durango Dgo.<br></div>
</div>
<div class="gallery">
  <a target="_blank" href="img\editorial\7.jpg">
    <img src="img\editorial\7.jpg" alt="Mountains" width="600" height="400">
  </a>
  <div class="desc"> Jasmin <br> Sombrerete Zac.<br></div>
</div>
<div class="gallery">
  <a target="_blank" href="img\editorial\8.jpg">
    <img src="img\editorial\8.jpg" alt="Mountains" width="600" height="400">
  </a>
  <div class="desc"> Brenda <br> Zapopan Jal.<br></div>
</div> 
<div class="gallery">
  <a target="_blank" href="img\editorial\9.jpg">
    <img src="img\editorial\9.jpg" alt="Mountains" width="600" height="400">
  </a>
  <div class="desc"> Brenda <br> Sombrerete Zac.<br></div>
</div>
<div class="gallery">
  <a target="_blank" href="img\editorial\10.jpg">
    <img src="img\editorial\10.jpg" alt="Mountains" width="600" height="400">
  </a>
  <div class="desc"> Paola <br> Noria de San Pantaleon SomZac.<br></div>
</div>
<div class="gallery">
  <a target="_blank" href="img\editorial\11.jpg">
    <img src="img\editorial\11.jpg" alt="Mountains" width="600" height="400">
  </a>
  <div class="desc"> Flor <br> Durango Dgo.<br></div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>