<!DOCTYPE html>
<html lang="es-mx">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TESTIMONIOS</title>
    <!-- Bootstrap CSS -->
    <link rel="shortcut icon" href="img/icon_camara.png" type="image/x-icon">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Cinzel&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/header.css">
    <link rel="stylesheet" type="text/css" href="css/avatar.css">

    <style>
    div.gallery {
      margin: 10px;
      border: 5px solid #ccc;
      float: left;
      width: 300px;
    }

    div.gallery:hover {
      border: 1px solid #777;
    }

    div.gallery img {
      width: 100%;
      height: auto;
    }

    div.desc {
      padding: 15px;
      text-align: center;
      font-family: 'Cinzel', serif;
    }

    div.a {
      text-align: center;
      font-family: 'Cinzel', serif;
    }
  </style>
</head>

<body>
    <header>
        <?php
        include 'header.php'
        ?>
    </header>

    <div class="a">
    <h2 > Testimonios </h2>
    </div>

    <!-- <div class="card imgcontainer" style="width: 18rem;">
        <img src="img/Paola-2.png" class="card-img-top avatar mx-auto" alt="avatar">
        <div class="card-body">
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
        </div>
    </div> -->
    <div class="a">
  
    <div class="row row-cols-1 row-cols-md-4 g-4 mx-5">
        <div class="col">
            <div class="card imgcontainer " style="width: 18rem;">
                <img src="img/testimonios/1.jpg" class="card-img-top avatar mx-auto" alt="...">
                <div class="card-body">
                    <!-- <h5 class="card-title">Card title</h5> -->
                    <p class="card-text header-st">100% recomendados, muchas gracias por todo.  Me encanta su trabajo muchísimo.</p>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card imgcontainer" style="width: 18rem;">
                <img src="img/testimonios/2.jpg" class="card-img-top avatar mx-auto" alt="...">
                <div class="card-body">
                    <!-- <h5 class="card-title">Card title</h5> -->
                    <p class="card-text header-st" >Muchas gracias, me encantaron mucho las fotos, me hicieron sentir muy bien.</p>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card imgcontainer" style="width: 18rem;">
                <img src="img/testimonios/3.jpg" class="card-img-top avatar mx-auto" alt="...">
                <div class="card-body">
                    <!-- <h5 class="card-title">Card title</h5> -->
                    <p class="card-text header-st">Muchas felicidades, hacen un trabajo fascinante me gusto mucho.</p>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card imgcontainer" style="width: 18rem;">
                <img src="img/testimonios/4.jpg" class="card-img-top avatar mx-auto" alt="...">
                <div class="card-body">
                    <!-- <h5 class="card-title">Card title</h5> -->
                    <p class="card-text header-st">Me encanta su trabajo, Muchas gracias por tomarme estas fotos.</p>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card imgcontainer" style="width: 18rem;">
                <img src="img/testimonios/5.jpg" class="card-img-top avatar mx-auto" alt="...">
                <div class="card-body">
                    <!-- <h5 class="card-title">Card title</h5> -->
                    <p class="card-text header-st">Me gusta muchisimo en su estilo y como trabajan, muchisimas gracias por todo.</p>
                </div>
            </div>
        </div>
    </div>


    </div>

    <?php
    include 'footer.php'
    ?>

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>

</html>