<html>

<head>
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Cinzel&display=swap" rel="stylesheet">
  <!-- ----------------------componentes ecenciales------------------------------------------- -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Bootstrap CSS -->
  <link rel="shortcut icon" href="img/icon_camara.png" type="image/x-icon">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="css/estilos.css">
  <link rel="stylesheet" type="text/css" href="css/header.css">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Cinzel&display=swap" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <!-- --------------------------------------- -->

  <style>
    div.gallery {
      margin: 10px;
      border: 5px solid #ccc;
      float: left;
      width: 300px;
    }

    div.gallery:hover {
      border: 1px solid #777;
    }

    div.gallery img {
      width: 100%;
      height: auto;
    }

    div.desc {
      padding: 15px;
      text-align: center;
      font-family: 'Cinzel', serif;
    }

    div.a {
      text-align: center;
      font-family: 'Cinzel', serif;
    }
  </style>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

  <link rel="stylesheet" type="text/css" href="css/estilos.css">
</head>

<body>
  <?php
  include 'header.php'
  ?>

  <div class="a">
    <h2> Bautizos </h2>
  </div>

  <div class="gallery">
    <a target="_blank" href="img\bautizos\abril.jpg">
      <img src="img\bautizos\abril.jpg" alt="1" width="600" height="400">
    </a>
    <div class="desc">Abril <br> Sombrerete Zac.</div>
  </div>
  <div class="gallery">
    <a target="_blank" href="img\bautizos\daria.jpg">
      <img src="img\bautizos\daria.jpg" alt="1" width="600" height="400">
    </a>
    <div class="desc">Daria <br> Sombrerete Zac.</div>
  </div>
  <div class="gallery">
    <a target="_blank" href="img\bautizos\hector.jpg">
      <img src="img\bautizos\hector.jpg" alt="1" width="600" height="400">
    </a>
    <div class="desc">Hector <br>Durango Dgo.</div>
  </div>
  <div class="gallery">
    <a target="_blank" href="img\bautizos\keidany.jpg">
      <img src="img\bautizos\keidany.jpg" alt="1" width="600" height="400">
    </a>
    <div class="desc">Keidany <br> Sombrerete Zac.</div>
  </div>
  <div class="gallery">
    <a target="_blank" href="img\bautizos\sebastian.jpg">
      <img src="img\bautizos\sebastian.jpg" alt="1" width="600" height="400">
    </a>
    <div class="desc">Sebastian <br> Sombrerete Zac.</div>
  </div>
  <div class="gallery">
    <a target="_blank" href="img\bautizos\vic.jpg">
      <img src="img\bautizos\vic.jpg" alt="1" width="600" height="400">
    </a>
    <div class="desc">Victoria <br>Guadalajara Jal.</div>
  </div>
  <div class="gallery">
    <a target="_blank" href="img\bautizos\yuletzy.jpg">
      <img src="img\bautizos\yuletzy.jpg" alt="1" width="600" height="400">
    </a>
    <div class="desc">Yuletzy <br> Sombrerete Zac.</div>
  </div>
  <div class="gallery">
    <a target="_blank" href="img\bautizos\nico.jpg">
      <img src="img\bautizos\nico.jpg" alt="1" width="600" height="400">
    </a>
    <div class="desc">Nicolas <br>Guadalajara Jal.</div>
  </div>
  <div class="gallery">
    <a target="_blank" href="img\bautizos\keidai1.jpg">
      <img src="img\bautizos\keidani1.jpg" alt="1" width="600" height="400">
    </a>
    <div class="desc">Keidany <br> Sombrerete Zac.</div>
  </div>
  <div class="gallery">
    <a target="_blank" href="img\bautizos\juampablo.jpg">
      <img src="img\bautizos\juampablo.jpg" alt="1" width="600" height="400">
    </a>
    <div class="desc">Juan Pablo <br>Durango Dgo.</div>
  </div>
  <div class="gallery">
    <a target="_blank" href="img\bautizos\ailen.jpg">
      <img src="img\bautizos\ailen.jpg" alt="1" width="600" height="400">
    </a>
    <div class="desc">Ailen <br> Sombrerete Zac.</div>
  </div>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>

</html>