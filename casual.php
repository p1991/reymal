<html>
<head>
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Cinzel&display=swap" rel="stylesheet">
<!-- ----------------------componentes ecenciales------------------------------------------- -->
<link rel="shortcut icon" href="img/icon_camara.png" type="image/x-icon">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Bootstrap CSS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="css/estilos.css">
  <link rel="stylesheet" type="text/css" href="css/header.css">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Cinzel&display=swap" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <!-- --------------------------------------- -->
<style>

div.gallery {
  margin: 10px;
  border: 5px solid #ccc;
  float: left;
  width: 300px;
}
div.gallery:hover {
  border: 1px solid #777;
}

div.gallery img {
  width: 100%;
  height: auto;
}

div.desc {
  padding: 15px;
  text-align: center;
  font-family: 'Cinzel', serif;
}
div.a { 
    text-align: center;
    font-family: 'Cinzel', serif;
}

</style>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
     integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" 
     crossorigin="anonymous">
    
    <link rel="stylesheet" type="text/css" href="css/estilos.css">
</head>

<body>
<?php
        include 'header.php'
?>

<div class="a">
<h2 > Casual </h2>
</div>

<div class="gallery" >
  <a target="_blank" href="img\casual\1.jpg">
    <img src="img\casual\1.jpg" alt="1" width="600" height="400">
  </a>
  <div class="desc">Tania<br> Sombrerete Zac.</div>
</div>

<div class="gallery">
  <a target="_blank" href="img\casual\2.jpg">
    <img src="img\casual\2.jpg" alt="2" width="600" height="400">
  </a>
  <div class="desc">Alexia Ramirez<br> Sombrerete Zac.</div>
</div>

<div class="gallery">
  <a target="_blank" href="img\casual\3.jpg">
    <img src="img\casual\3.jpg" alt="3" width="600" height="400">
  </a>
  <div class="desc">Emili<br> Sierra de Organos</div>
</div>

<div class="gallery">
  <a target="_blank" href="img\casual\4.jpg">
    <img src="img\casual\4.jpg" alt="Mountains" width="600" height="400">
  </a>
  <div class="desc">Nayla<br> Durango Dgo.</div>
</div>
<div class="gallery">
  <a target="_blank" href="img\casual\5.jpg">
    <img src="img\casual\5.jpg" alt="Mountains" width="600" height="400">
  </a>
  <div class="desc">Jimena<br> Durango Dgo.</div>
</div>
<div class="gallery">
  <a target="_blank" href="img\casual\6.jpg">
    <img src="img\casual\6.jpg" alt="Mountains" width="600" height="400">
  </a>
  <div class="desc">Valeria<br> Canatlan Dgo. </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>