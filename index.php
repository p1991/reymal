<!DOCTYPE html>
<html lang="en-mx">
<style>
  div.gallery {
    margin: 100px;
    border: 5px solid #ccc;
    float: left;
    width: 400px;
  }

  div.gallery:hover {
    border: 1px solid #777;
  }

  div.gallery img {
    width: 100%;
    height: auto;
  }

  div.desc {
    padding: 15px;
    text-align: center;
    font-family: 'Cinzel', serif;
  }

  div.a {
    text-align: center;
    font-family: 'Cinzel', serif;
  }
</style>

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Brayan Reyes - Fotografia</title>
  <link rel="shortcut icon" href="img/icon_camara.png" type="image/x-icon">
  <!-- Bootstrap CSS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

  <link rel="stylesheet" type="text/css" href="css/estilos.css">
  <link rel="stylesheet" type="text/css" href="css/header.css">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Cinzel&display=swap" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

  <!-- CSS -->
  <link rel="stylesheet" type="text/css" href="css/Index.css">
</head>

<body>
  <header>
    <?php
    include 'header.php'
    ?>
  </header>

  <div id="carouselExampleIndicators" style="margin-top: 20px; margin-right: 20px; margin-left: 20px;" class="carousel slide" data-bs-ride="carousel">
    <div class="carousel-indicators">
      <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
      <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
      <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
      <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="3" aria-label="Slide 4"></button>
      <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="4" aria-label="Slide 5"></button>
      <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="5" aria-label="Slide 6"></button>
      <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="6" aria-label="Slide 7"></button>
      <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="7" aria-label="Slide 8"></button>
      <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="8" aria-label="Slide 9"></button>
    </div>
    <div class="carousel-inner " >
      <div class="carousel-item active " >
        <img src="img\slider\1.png"  class="d-block w-100 h-100 " alt="...">
      </div>
      <div class="carousel-item ">
        <img src="img\slider\2.png" class="d-block  w-100 h-100 "  alt="...">
      </div>
      <div class="carousel-item">
        <img src="img\slider\3.png" class="d-block  w-100 h-100 " alt="...">
      </div>
      <div class="carousel-item">
        <img src="img\slider\4.png" class="d-block  w-100 h-100 " alt="...">
      </div>
      <div class="carousel-item">
        <img src="img\slider\5.png" class="d-block  w-100 h-100 " alt="...">
      </div>
      <div class="carousel-item">
        <img src="img\slider\6.png" class="d-block  w-100 h-100 " alt="...">
      </div>
      <div class="carousel-item">
        <img src="img\slider\7.png" class="d-block  w-100 h-100 " alt="...">
      </div>
      <div class="carousel-item">
        <img src="img\slider\8.png" class="d-block  w-100 h-100 " alt="...">
      </div>
      <div class="carousel-item">
        <img src="img\slider\9.png" class="d-block  w-100 h-100 " alt="...">
      </div>
    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Next</span>
    </button>
  </div>
  <div class="centro">
    <h2> Nuestro Estilo </h2>
    <h6>La escencia de nuestro trabajo se encuentra <br></h6>
    <h6>en capturar los momentos mas importantes de <br></h6>
    <h6>tu vida con una imagen digna de una revista. <br></h6>
  </div>



  <div class="container mt-3">
    <div class="brayan">
      <img src="img\Paola.jpg" alt="Paris" width="250" height="auto">
      <img src="img\susana.jpg" alt="Paris" width="250" height="auto">
      <img src="img\aaa.jpg" alt="Paris" width="250" height="auto">
      <img src="img\e.jpg" alt="Paris" width="250" height="auto">
    </div>
  </div>


  <div class="gallery">
    <a target="_blank" href="img\byc.jpg">
      <img src="img\byc.jpg" alt="1" width="600" height="400">
    </a>
  </div>
  <div class="der">
    <h2> Acerca de Nosotros </h2>
    <h6>Somos dos jovenes Fotografos apasionados<br></h6>
    <h6>por contar historias con nuestro trabajo.<br></h6>
    <h6>Disfrutamos de la naturaleza, los paisajes <br></h6>
    <h6>y la luz del sol. Viendose claramente<br></h6>
    <h6>reflejado en nuestro trabajo, con visiones<br></h6>
    <h6>muy distinas, para nosotros la fotografia es <br></h6>
    <h6>una fornma de revivir el pasado, una forma<br></h6>
    <h6>de volver a sentir y conectar con las personas.<br></h6>
  </div>

  <?php
  include 'footer.php'
  ?>

  <!-- Option 1: Bootstrap Bundle with Popper -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>


</body>

</html>