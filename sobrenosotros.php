<html>
<head>
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Cinzel&display=swap" rel="stylesheet">
<!-- ----------------------componentes ecenciales------------------------------------------- -->
<meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Bootstrap CSS -->
  <link rel="shortcut icon" href="img/icon_camara.png" type="image/x-icon">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="css/estilos.css">
  <link rel="stylesheet" type="text/css" href="css/header.css">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Cinzel&display=swap" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <!-- --------------------------------------- -->
<style>

div.gallery {
  margin: 10px;
  border: 5px solid #ccc;
  float: left;
  width: 300px;
}
div.gallery:hover {
  border: 1px solid #777;
}

div.gallery img {
  width: 100%;
  height: auto;
}

div.desc {
  padding: 15px;
  text-align: center;
  font-family: 'Cinzel', serif;
}
div.a { 
    text-align: center;
    font-family: 'Cinzel', serif;
}

</style>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
     integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" 
     crossorigin="anonymous">
    
    <link rel="stylesheet" type="text/css" href="css/estilos.css">
</head>

<body>
    <header>
        <?php
            include 'header.php'
         ?>
    
    </header>

<div class="a">
<h2 > Sobre Nosotros </h2>
  <div class="container mt-3">
    <img class="img-fluid" src="img\byc.jpg" alt="byc" width="500" height="auto">
      <h6>Somos dos jovenes Fotografos apasionados<br></h6>
      <h6>por contar historias con nuestro trabajo.<br></h6>
      <h6>Disfrutamos de la naturaleza, los paisajes <br></h6>
      <h6>y la luz del sol. Viendose claramente<br></h6>
      <h6>reflejado en nuestro trabajo, con visiones<br></h6>
      <h6>muy distinas, para nosotros la fotografia es <br></h6>
      <h6>una fornma de revivir el pasado, una forma<br></h6>
      <h6>de volver a sentir y conectar con las personas.<br></h6>
  </div>
</div>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<?php
  include 'footer.php'
  ?>

</body>
</html>



