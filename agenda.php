<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Agenda</title>
  <link rel="shortcut icon" href="img/icon_camara.png" type="image/x-icon">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="css/estilos.css">
  <link rel="stylesheet" type="text/css" href="css/header.css">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Cinzel&display=swap" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

  <link rel="stylesheet" type="text/css" href="css/agenda.css">

  <style>
    div.a {
      
      font-family: 'Cinzel', serif;
    }
  </style>
</head>

<body>
  <header>
    <?php
    include 'header.php'
    ?>
  </header>
  <?php
        include 'agendar.php'
       ?>
  
  <div class="a">
    
  <div id="general">
    <div id="formbox">
      <h2>AGENDA TU SESIÓN</h2>
      <form method="post" class="row g-3 needs-validation">
        <div>
          <div  class="mb-3">
            <label class="form-label">Nombre:</label>
            <div>
              <input name="nombre" type="text" require class="form-control">
            </div>
          </div>
          
          <div class="mb-3">
            <label class="form-label">Fecha:</label>

            <input name="fecha" type="date" id="start" value="" class="form-control">

          </div>
          <div class="mb-3">
            <label class="form-label">Paquete:</label>

            <select name="paquete" class="form-select" id="inlineFormSelectPref">
              
              <option value="Bautizo Basico">Bautizo Basico</option>
              <option value="Bautizo Plus">Bautizo Plus</option>
              <option value="Bautizo">Bautizo</option>
              <option value="Boda 1<">Boda 1</option>
              <option value="Boda 2">Boda 2</option>
              <option value="Casual 1">Casual 1</option>
              <option value="Casual 2">Casual 2</option>
              <option value="Casual 3<">Casual 3</option>
              <option value="Embarazo 1">Embarazo 1</option>
              <option value="Embarazo 2">Embarazo 2</option>
              <option value="Embarazo 3">Embarazo 3</option>
              <option value="Infaltil 1">Infaltil 1</option>
              <option value="Infaltil 2">Infaltil 2</option>
              <option value="XV Años Basico">XV Años Basico</option>
              <option value="XV Años Plus">XV Años Plus</option>
              <option value="XV Años By Succes Basico">XV Años By Succes Basico </option>
              <option value="XV Años By Succes Premium">XV Años By Succes Premium </option>
              <option value="XV Años By Succes Clasico">XV Años By Succes Clasico </option>
              <option value="Pareja 1">Pareja 1</option>
              <option value="Pareja 2">Pareja 2</option>
              <option value="Productos 1">Productos 1</option>
              <option value="Productos 2">Productos 2</option>
              <option value="Productos 3">Productos 3</option>
            </select>

          </div>
          <div class="mb-3">
            <label class="form-label">Correo:</label>
            <div>
              <input name="email" type="Email" require class="form-control">
            </div>
          </div>
          <div class="mb-3">
            <label class="form-label">Telefono:</label>
            <div>
              <input name="telefono" type="number" require class="form-control">
            </div>
          </div>
          <button class="btn btn-primary" type="submit" name="agendar">
            AGENDAR
          </button>
        </div>
      </form>
      
    </div>
    <div id="imgbox">
      <img src="img/editorial/1a.jpg" id="imgform">
    </div>
  </div>

  </div>



<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
<footer>
  <?php
  include 'footer.php'
  ?>
</footer>
</html>