<html>
<head>
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Cinzel&display=swap" rel="stylesheet">
<!-- ----------------------componentes ecenciales------------------------------------------- -->
<meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Bootstrap CSS -->
  <link rel="shortcut icon" href="img/icon_camara.png" type="image/x-icon">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="css/estilos.css">
  <link rel="stylesheet" type="text/css" href="css/header.css">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Cinzel&display=swap" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <!-- ----------------------------------------->
<style>

div.gallery {
  margin: 10px;
  border: 5px solid #ccc;
  float: left;
  width: 300px;
}
div.gallery:hover {
  border: 1px solid #777;
}

div.gallery img {
  width: 100%;
  height: auto;
}

div.desc {
  padding: 15px;
  text-align: center;
  font-family: 'Cinzel', serif;
}
div.a { 
    text-align: center;
    font-family: 'Cinzel', serif;
}

</style>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
     integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" 
     crossorigin="anonymous">
    
    <link rel="stylesheet" type="text/css" href="css/estilos.css">
</head>

<body>
<?php
        include 'header.php'
?>

<div class="a">
<h2 > XV Años </h2>
</div>

<div class="gallery" >
  <a target="_blank" href="img\XV\1.jpg">
    <img src="img\XV\1.jpg" alt="1" width="600" height="400">
  </a>
  <div class="desc"> Tania <br> Sombrerete Zac.<br>06.11.2021</div>
</div>

<div class="gallery">
  <a target="_blank" href="img\XV\2.jpg">
    <img src="img\XV\2.jpg" alt="2" width="600" height="400">
  </a>
  <div class="desc">Alicia<br>Durango Dgo.<br>18.06.2021</div>
</div>

<div class="gallery">
  <a target="_blank" href="img\XV\3.jpg">
    <img src="img\XV\3.jpg" alt="3" width="600" height="400">
  </a>
  <div class="desc">Emily<br>Sierra de Organos<br>07.11.2021</div>
</div>

<div class="gallery">
  <a target="_blank" href="img\XV\4.jpg">
    <img src="img\XV\4.jpg" alt="Mountains" width="600" height="400">
  </a>
  <div class="desc">Pamela<br>La batea<br>24.04.2021</div>
</div>
<div class="gallery">
  <a target="_blank" href="img\XV\5.jpg">
    <img src="img\XV\5.jpg" alt="Mountains" width="600" height="400">
  </a>
  <div class="desc">Alexia <br>San jose de Felix Som.Zac<br>18.09.2021</div>
</div>
<div class="gallery">
  <a target="_blank" href="img\XV\6.jpg">
    <img src="img\XV\6.jpg" alt="Mountains" width="600" height="400">
  </a>
  <div class="desc">Adilene<br>Nombre de Dios Dgo.<br>31.07.2021</div>
</div>
<div class="gallery">
  <a target="_blank" href="img\XV\7.jpg">
    <img src="img\XV\7.jpg" alt="Mountains" width="600" height="400">
  </a>
  <div class="desc">Jeidy<br> Sombrerete Zac.<br>13.10.2021</div>
  
</div>
<div class="gallery">
  <a target="_blank" href="img\XV\8.jpg">
    <img src="img\XV\8.jpg" alt="Mountains" width="600" height="400">
  </a>
  <div class="desc">Valeria<br>Canatlan Dgo.<br>24.07.2021</div>
</div> 
<div class="gallery">
  <a target="_blank" href="img\XV\9.jpg">
    <img src="img\XV\9.jpg" alt="Mountains" width="600" height="400">
  </a>
  <div class="desc">Jimena<br>Durango Dgo.<br>08.10.2021</div>
</div>
<div class="gallery">
  <a target="_blank" href="img\XV\10.jpg">
    <img src="img\XV\10.jpg" alt="Mountains" width="600" height="400">
  </a>
  <div class="desc">Alejandra <br>Vicente Guerrero Dgo.<br>31.12.2020</div>
</div>
<div class="gallery">
  <a target="_blank" href="img\XV\11.jpg">
    <img src="img\XV\11.jpg" alt="Mountains" width="600" height="400">
  </a>
  <div class="desc">Camila<br>Vicente Guerrero Dgo.<br>01.05.2025</div>
</div>
<div class="gallery">
  <a target="_blank" href="img\XV\12.jpg">
    <img src="img\XV\12.jpg" alt="Mountains" width="600" height="400">
  </a>
  <div class="desc">Fernanda <br> Sombrerete Zac.<br>31.10.2021</div>
</div>
<div class="gallery">
  <a target="_blank" href="img\XV\13.jpg">
    <img src="img\XV\13.jpg" alt="Mountains" width="600" height="400">
  </a>
  <div class="desc">Alexia<br> Sombrerete Zac.<br>31.07.2021</div>
</div>
<div class="gallery">
  <a target="_blank" href="img\XV\14.jpg">
    <img src="img\XV\14.jpg" alt="Mountains" width="600" height="400">
  </a>
  <div class="desc"> ...<br> Sierra De Organos.<br>17.10.2021</div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>