<html>
<head>
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Cinzel&display=swap" rel="stylesheet">
<!-- ----------------------componentes ecenciales------------------------------------------- -->
<meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Bootstrap CSS -->
  <link rel="shortcut icon" href="img/icon_camara.png" type="image/x-icon">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="css/estilos.css">
  <link rel="stylesheet" type="text/css" href="css/header.css">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Cinzel&display=swap" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <!-- --------------------------------------- -->
<style>

div.gallery {
  margin: 10px;
  border: 5px solid #ccc;
  float: left;
  width: 300px;
}

div.gallery:hover {
  border: 1px solid #777;
}

div.gallery img {
  width: 100%;
  height: auto;
}

div.desc {
  padding: 15px;
  text-align: center;
  font-family: 'Cinzel', serif;
}
div.a { 
    text-align: center;
    font-family: 'Cinzel', serif;
}

</style>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
     integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" 
     crossorigin="anonymous">
    
    <link rel="stylesheet" type="text/css" href="css/estilos.css">
</head>

<body>
<?php
        include 'header.php'
?>

<div class="a">
<h2 > Bodas </h2>
</div>

<div class="gallery" >
  <a target="_blank" href="img\Bodas\1.jpg">
    <img src="img\Bodas\1.jpg" alt="1" width="600" height="400">
  </a>
  <div class="desc">Mayra & Rogelio <br> Sombrerete Zac.<br>11.09.2021</div>
</div>

<div class="gallery">
  <a target="_blank" href="img\Bodas\2.jpg">
    <img src="img\Bodas\2.jpg" alt="2" width="600" height="400">
  </a>
  <div class="desc">Mayra & Rogelio <br> Sombrerete Zac.<br>11.09.2021</div>
</div>

<div class="gallery">
  <a target="_blank" href="img\Bodas\3.jpg"">
    <img src="img\Bodas\3.jpg" alt="3" width="600" height="400">
  </a>
  <div class="desc">Marlene & Diego <br> Sombrerete Zac.<br>23.11.2020</div>
</div>

<div class="gallery">
  <a target="_blank" href="img\Bodas\4.jpg"">
    <img src="img\Bodas\4.jpg" alt="4" width="600" height="400">
  </a>
  <div class="desc">Marlene & Diego <br> Sombrerete Zac.<br>23.11.2020</div>
</div>
<div class="gallery">
  <a target="_blank" href="img\Bodas\5.jpg"">
    <img src="img\Bodas\5.jpg" alt="5" width="600" height="400">
  </a>
  <div class="desc">Danitza & Arnold <br> Vicente Guerrero Dgo.<br>19.09.2020</div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>